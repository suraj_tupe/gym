<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUseridUserInformationTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('user_informations', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->after('id');
            $table->string('first_name')->after('user_id');
            $table->string('last_name')->after('first_name');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('user_informations', function (Blueprint $table) {
            $table->dropColumn(['user_id']);
            $table->dropColumn(['first_name']);
            $table->dropColumn(['last_name']);
        });
    }

}
