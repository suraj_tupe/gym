<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mobile');
            $table->text('address');
            $table->date('dob');
//            $table->enum('payment_mode',['0','1','2'])->comment('0=>Cash,1=>Card,2=>other')->default('0');
//            $table->string('other_payment_mode')->nullable();
//            $table->float('total_fees',8,2);
//            $table->float('paid_amount',8,2);
//            $table->float('balance_amount',8,2)->nullable();;
//            $table->float('discount_amount',8,2)->nullable();;
//            $table->date('from_date');
//            $table->date('to_date');
//            $table->integer('duration');
//            $table->enum('offer_type',['0','1','2'])->comment('0=>Gym,1=>Gym+cardio,2=>Gym+Cardio+Crossfit')->default('0');
            $table->enum('offer_opted_for',['0','1','2'])->comment('0=>Single,1=>Couple,2=>Group')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_informations');
    }
}
