<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransInformationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('trans_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('information_id')->unsigned();
            $table->integer('offer_type');
            $table->enum('payment_mode', ['0', '1', '2'])->comment('0=>Cash,1=>Card,2=>other')->default('0');
            $table->string('other_payment_mode')->nullable();
            $table->float('total_fees', 8, 2);
            $table->float('paid_amount', 8, 2);
            $table->float('balance_amount', 8, 2)->nullable();
            $table->float('discount_amount', 8, 2)->nullable();
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('duration');
            $table->timestamps();
        });
        Schema::table('trans_informations', function (Blueprint $table) {
            $table->foreign('information_id')->references('id')->on('user_informations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('trans_informations');
    }

}
