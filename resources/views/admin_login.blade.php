@extends('layouts.admin_login')
@section('content')
<div class="page login-page">
    <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
            <div class="row">
                <!-- Logo & Information Panel-->
                <div class="col-lg-6">
                    <div class="info d-flex align-items-center">
                        <div class="content">
                            <div class="logo">
                                <h1>Admin</h1>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        </div>
                    </div>
                </div>
                <!-- Form Panel    -->
                <div class="col-lg-6 bg-white">
                    <div class="form d-flex align-items-center">
                        <div class="content">
                            <form id="login-form" method="post" action="{{ url('admin-login') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" type="text" name="email" class="input-material">
                                    <label for="login-username" class="label-material">Email Address</label>
                                    @if ($errors->has('email'))
                                    <div class="text-danger">
                                        {{ $errors->first('email') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" name="password"  class="input-material">
                                    <label for="login-password" class="label-material">Password</label>
                                    @if ($errors->has('password'))
                                    <div class="text-danger">
                                       {{ $errors->first('password') }}
                                    </div>
                                    @endif
                                </div>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} style="opacity:0;">
                                       <input type="submit" name="btn_login" id="btn_login" value="Login" class="btn btn-primary" />
                            </form>
                            <a href="#" class="forgot-pass">Forgot Password?</a><br><small>Do not have an account? </small>
                            <a href="#" class="signup">Signup</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyrights text-center">
        <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
    </div>
</div>
@endsection
