@extends('layouts.admin')
@section('content')
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Edit Global Setting</h2>
    </div>
</header>
<!-- Dashboard Counts Section-->
<ul class="breadcrumb">
    <div class="container-fluid">
        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{url('/manage-users')}}">User List</a></li>
        <li class="breadcrumb-item active">Edit User</li>
    </div>
</ul>
<section class="dashboard-counts no-padding-bottom">
    <div class="container-fluid">
        <div class="row bg-white has-shadow">
            <div class="card-body">
                <form class="form-horizontal" action="{{url('/edit-user-post/'.base64_encode($edit_id))}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">First name</label>
                        <div class="col-sm-8">
                            <input type="text" name="first_name" id="first_name" class="form-control" value="{{$user_details->userInfo->first_name}}">
                        </div>

                        @if ($errors->has('value'))
                        <div class="text-danger">{{$errors->first('first_name')}}</div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Last name</label>
                        <div class="col-sm-8">
                            <input type="text" name="last_name" id="last_name" class="form-control" value="{{$user_details->userInfo->last_name}}">
                        </div>
                        @if ($errors->has('last_name'))
                        <div class="text-danger">{{$errors->first('last_name')}}</div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="text" name="user_email" id="user_email" class="form-control" value="{{$user_details->email}}">
                            <small>(Optional)</small>
                            @if ($errors->has('user_email'))
                            <div class="text-danger">{{$errors->first('user_email')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Mobile Number</label>
                        <div class="col-sm-8">
                            <input type="text" name="mobile_number" id="mobile_number" class="form-control" value="{{$user_details->userInfo->mobile}}">
                        </div>
                        @if ($errors->has('mobile_number'))
                        <div class="text-danger">{{$errors->first('mobile_number')}}</div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Date of Birth</label>
                        <div class="col-sm-8">
                            <input type="text" name="dob" id="dob" class="form-control" value="{{date('d-m-Y',strtotime($user_details->userInfo->dob))}}">
                        </div>
                        @if ($errors->has('dob'))
                        <div class="text-danger">{{$errors->first('dob')}}</div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Address</label>
                        <div class="col-sm-8">
                            <textarea name="address" id="address" class="form-control">{{$user_details->userInfo->address}}</textarea>
                        </div>
                        @if ($errors->has('address'))
                        <div class="text-danger">{{$errors->first('address')}}</div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Training Type<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">

                            <input type="radio" class="training_type" name="training_type" id="training_type" value="0"  @if($user_details->userInfo->transInfo->first()->gymInfo->first()->gym_type==0) checked="" @endif> Normal
                                   <input type="radio" class="training_type" name="training_type" id="training_type" value="1"   @if($user_details->userInfo->transInfo->first()->gymInfo->first()->gym_type==1) checked="" @endif> Personal
                                   @if ($errors->has('training_type'))
                                   <div class="text-danger">{{$errors->first('training_type')}}</div>
                            @endif
                        </div>
                    </div>
                    <hr/>
                    @php
                    $last_date='';
                    @endphp
                    @if(count($user_details->userInfo->transInfo)>0)
                    @foreach($user_details->userInfo->transInfo as $trans_info)
                    @php
                    $last_date=$trans_info->to_date;
                    @endphp
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Payment Mode</label>
                        <div class="col-sm-8">
                            <input type="radio" disabled=""  value="0" @if($trans_info->payment_mode == 0) checked=""  @endif> Cash
                                   <input type="radio"  disabled=""  value="1" @if($trans_info->payment_mode == 1) checked="" @endif> Card
                                   <input type="radio"  disabled=""  value="2" @if($trans_info->payment_mode == 2) checked="" @endif> Other
                                   <input type="text" class="form-control" disabled=""  @if($trans_info->payment_mode == 2) value="{{$trans_info->other_payment_mode}}"   @else  style="display:none" @endif/>
                        </div>
                        @if ($errors->has('payment_mode'))
                        <div class="text-danger">{{$errors->first('payment_mode')}}</div>
                        @endif
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Duration<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            @if(count($durations)>0)
                            @foreach($durations as $key=>$d)
                            <input type="radio"  disabled=""  value="{{$d->duration}}" @if($trans_info->duration==$d->duration) checked="" @endif> {{ ($d->duration==365)?'12':($d->duration)/30}} Month
                                   @endforeach
                                   @endif
                                   @if ($errors->has('payment_mode'))
                                   <div class="text-danger">{{$errors->first('payment_mode')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Offer Type</label>
                        <div class="col-sm-8">
                            @php
                            $gym_type = $trans_info->gymInfo->gym_type;

                            $gym_records = App\Package::where('gym_type', $gym_type)->where('duration', $trans_info->gymInfo->duration)->groupBy('name')->get();
                            @endphp
                            @if(count($gym_records)>0)
                            @foreach($gym_records as $key=>$gym)
                            <input type="radio"  @if(isset($trans_info->offer_type)) disabled="" @endif  value="{{$gym->id}}"   @if($gym->id==$trans_info->offer_type) checked="" @endif> {{$gym->name}}
                                   @endforeach
                                   @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Total Amount</label>
                        <div class="col-sm-8">
                            <input type="text"  readonly="" class="form-control" value="{{$trans_info->total_fees}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Discount Amount</label>
                        <div class="col-sm-8">
                            <input type="text"  class="form-control" readonly="" value="{{$trans_info->discount_amount}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Balance Amount</label>
                        <div class="col-sm-8">
                            <input type="text"  readonly="" class="form-control" value="{{$trans_info->balance_amount}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">From Date</label>
                        <div class="col-sm-8">
                            <input type="text"  class="form-control" value="{{date('d-m-Y',strtotime($trans_info->from_date))}}" readonly="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">To Date</label>
                        <div class="col-sm-8">
                            <input type="text"  class="form-control" value="{{date('d-m-Y',strtotime($trans_info->to_date))}}" readonly="">
                        </div>
                    </div>
                      <hr>
                    @endforeach
                    @if (strtotime($last_date) <= strtotime(date('Y-m-d')))
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <a class=" btn-success btn-sm" id="renew_membership" href="javascript:void(0);">Renew</a>
                        </div>
                    </div>
                    @endif
                    @endif
                  
                    <div id="html_div"></div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Offer Opted For</label>
                        <div class="col-sm-8">
                            <input type="radio" name="offer_opted_for" id="offer_opted_for" value="0" @if($user_details->userInfo->offer_opted_for==0) checked="" @endif> Single
                                   <input type="radio" name="offer_opted_for" id="offer_opted_for" value="1" @if($user_details->userInfo->offer_opted_for==1) checked="" @endif> Couple
                                   <input type="radio" name="offer_opted_for" id="offer_opted_for" value="2" @if($user_details->userInfo->offer_opted_for==2) checked="" @endif> Group
                        </div>
                        @if ($errors->has('offer_opted_for'))
                        <div class="text-danger">{{$errors->first('offer_opted_for')}}</div>
                        @endif
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                        <div class="col-sm-12 offset-sm-3">
                            <a href="{{url('/manage-users')}}" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-primary">Save changes</button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    $(document.body).on('change', '.payment_mode', function () {
        var v = $(this).val();
        if (v == 2) {
            $('#other_type').show();
        } else {
            $('#other_type').val('');
            $('#other_type').hide();
        }
    });
    $(document).ready(function () {

        $("#to_date").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date('{{date("Y-m-d")}}'),
            onSelect: function (selected) {
                var dt1 = new Date(selected);
                dt1.setDate(dt1.getDate() - 1);
                $("#from_date").datepicker("option", "maxDate", dt1);
            }
        });
        $("#dob").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: "-70:+0",
        });
        $(document.body).on('change', '.offer-type-cls', function () {
            var offer_type = $('.offer-type-cls:checked').val();
            getOfferType(offer_type);
        });
        $(document.body).on('change', '#paid_amount', function () {
            var total_amount = $('#discount_amount').val();
            var paid_amount = $('#paid_amount').val();
            $('#balance_amount').val(total_amount - paid_amount);
        });
        $(document.body).on('change', '#discount_amount', function () {
            var total_amount = $('#discount_amount').val();
            var paid_amount = $('#paid_amount').val();
            if (paid_amount > 0) {
                $('#balance_amount').val(total_amount - paid_amount);
            }
        });
        $(document.body).on('change', '.training_type', function () {
            var gym_type = $('.training_type:checked').val();
            var duration = $('.duration:checked').val();
            getGymType(gym_type, duration);
        });
        $(document.body).on('change', '.duration', function () {
            var gym_type = $('.training_type:checked').val();
            var duration = $('.duration:checked').val();
            getGymType(gym_type, duration);
        });
    });
    function getOfferType(offer_type) {
        $('#total_amount').val('');
        $('#discount_amount').val('');
        $('#paid_amount').val('');
        $('#balance_amount').val('');
//        alert(offer_type);
        $.ajax({
            url: '{{url("/get-offer-info")}}',
            type: 'post',
            dataType: 'json',
            data: {
                'offer_id': offer_type,
                '_token': '{{ csrf_token() }}',
            },
            success: function (msg) {
                $('#total_amount').val(msg.price);
                $('#discount_amount').val(msg.price);
            }
        })
    }
    function getGymType(gym_type, duration) {
        $('#total_amount').val('');
        $('#discount_amount').val('');
        $('#paid_amount').val('');
        $('#balance_amount').val('');
        $.ajax({
            url: '{{url("/get-gym-info")}}',
            type: 'post',
            dataType: 'json',
            data: {
                'gym_type': gym_type,
                duration: duration,
                '_token': '{{ csrf_token() }}',
            },
            success: function (msg) {
                $('#offer_type_div').html(msg.viewInfo);
                $('#total_amount').val(msg.price);
                $('#discount_amount').val(msg.price);
            }
        })
    }

    $('#renew_membership').on('click', function () {

        $.ajax({
            url: '{{url("/get-renew")}}',
            type: 'post',
            dataType: 'json',
            data: {
                '_token': '{{csrf_token()}}',
            },
            success: function (msg) {
                $('#html_div').html(msg.viewInfo);
                var gym_type = $('.training_type:checked').val();
                var duration = $('.duration:checked').val();
                getGymType(gym_type, duration);
                $('#renew_membership').remove();
                $("#from_date").datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: new Date('{{date("Y-m-d")}}'),
                    onSelect: function (selected) {
                        var dt = new Date(selected);
                        dt.setDate(dt.getDate() + 1);
                        $("#to_date").datepicker("option", "minDate", dt);
                    }
                });
            }
        });



    });
</script>
@endsection