@extends('layouts.admin')
@section('content')
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">User List
            <a href="{{url('/add-user')}}" class="btn btn-primary float-right">Add User</a>
        </h2>
    </div>
</header>
<!-- Dashboard Counts Section-->
<section class="dashboard-counts no-padding-bottom">
    <div class="container-fluid">
        @include('layouts.message')
        <div class="row bg-white has-shadow">
            <div class="table-responsive">
                <table class="table table-bordered" id="user_list">
                    <thead>
                    <th>Index</th>
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <!--<th>Offer Type</th>-->
                    <!--<th>Paid Amount</th>-->
                    <!--<th>Balance Amount</th>-->
                    <th>Next Expiry Date</th>
                    <th>Action</th>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $('#user_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{url('/get-user-list')}}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'mobile', name: 'mobile'},
//                {data: 'offer_type', name: 'offer_type'},
//                {data: 'paid_amount', name: 'paid_amount'},
//                {data: 'balance_amount', name: 'balance_amount'},
                {data: 'to_date', name: 'to_date'},
                {data: 'action', name: 'action'},
            ],
        });


    });</script>
@endsection