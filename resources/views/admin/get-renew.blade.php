<div class="form-group row">
    <label class="col-sm-3 form-control-label">Payment Mode<i class="text-danger" >*</i></label>
    <div class="col-sm-8">
        <input type="radio" class="payment_mode" name="payment_mode" id="payment_mode" value="0" @if(old('payment_mode')==0) checked="" @elseif(old('payment_mode')=='')) checked="" @endif > Cash
               <input type="radio" class="payment_mode" name="payment_mode" id="payment_mode" value="1" @if(old('payment_mode')==1) checked="" @endif > Card
               <input type="radio" class="payment_mode" name="payment_mode" id="payment_mode" value="2"  @if(old('payment_mode')==2) checked="" @endif> Other
               <input type="text" class="form-control" name="other_type" id="other_type" style="display: none">
        @if ($errors->has('payment_mode'))
        <div class="text-danger">{{$errors->first('payment_mode')}}</div>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 form-control-label">Duration<i class="text-danger" >*</i></label>
    <div class="col-sm-8">
        @if(count($durations)>0)
        @foreach($durations as $key=>$d)
        <input type="radio" class="duration" name="duration" id="duration" value="{{$d->duration}}" @if($key==0) checked="" @endif> {{ ($d->duration==365)?'12':($d->duration)/30}} Month
               @endforeach
               @endif
               @if ($errors->has('payment_mode'))
               <div class="text-danger">{{$errors->first('payment_mode')}}</div>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 form-control-label">Offer Type<i class="text-danger" >*</i></label>
    <div class="col-sm-8" >
        <div id="offer_type_div" ></div>
        @if ($errors->has('offer_type'))
        <div class="text-danger">{{$errors->first('offer_type')}}</div>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 form-control-label">Total Amount<i class="text-danger" >*</i></label>
    <div class="col-sm-8">
        <input type="text" name="total_amount" id="total_amount" readonly="" class="form-control" >
        @if ($errors->has('total_amount'))
        <div class="text-danger">{{$errors->first('total_amount')}}</div>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 form-control-label">Discount Amount<i class="text-danger" >*</i></label>
    <div class="col-sm-8">
        <input type="text" name="discount_amount" id="discount_amount" class="form-control"  value="">
        @if ($errors->has('discount_amount'))
        <div class="text-danger">{{$errors->first('discount_amount')}}</div>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 form-control-label">Paid Amount<i class="text-danger" >*</i></label>
    <div class="col-sm-8">
        <input type="text" name="paid_amount" id="paid_amount" class="form-control">
        @if ($errors->has('discount_amount'))
        <div class="text-danger">{{$errors->first('discount_amount')}}</div>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 form-control-label">Balance Amount<i class="text-danger" >*</i></label>
    <div class="col-sm-8">
        <input type="text" name="balance_amount" id="balance_amount" class="form-control" readonly="">
        @if ($errors->has('balance_amount'))
        <div class="text-danger">{{$errors->first('balance_amount')}}</div>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 form-control-label">From Date<i class="text-danger" >*</i></label>
    <div class="col-sm-8">
        <input type="text" name="from_date" id="from_date" class="form-control" >
        @if ($errors->has('from_date'))
        <div class="text-danger">{{$errors->first('from_date')}}</div>
        @endif
    </div>
</div>
<hr/>