@extends('layouts.admin')
@section('content')
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Global Setting</h2>
    </div>
</header>
<!-- Dashboard Counts Section-->
<section class="dashboard-counts no-padding-bottom">
    <div class="container-fluid">
        @include('layouts.message')
        <div class="row bg-white has-shadow">
            <div class="table-responsive">
                <table class="table table-bordered" id="global_setting_list">
                    <thead>
                    <th>Index</th>
                    <th>Field</th>
                    <th>Value</th>
                    <th>Action</th>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function(){
    $('#global_setting_list').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{url('/get-global-setting')}}",
        columns: [
            { data: 'id', name: 'id' },
            { data: 'field', name: 'field' },
            { data: 'value', name: 'value' },
            { data: 'action', name: 'action' },
        ],
    });
    
    
});</script>
@endsection