@extends('layouts.admin')
@section('content')
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">User Report
        </h2>
    </div>
</header>
<!-- Dashboard Counts Section-->
<section class="dashboard-counts no-padding-bottom">
    <div class="container-fluid">
        @include('layouts.message')
        <div class="row bg-white has-shadow">
            <form class="form-horizontal" method="post" action="{{url('/post-report-details')}}" >
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">From Date<i class="text-danger" >*</i></label>
                    <div class="col-sm-8">
                        <input type="text" name="from_date" id="from_date" class="form-control" value="{{old('from_date')?old('from_date'):''}}" >
                        @if ($errors->has('from_date'))
                        <div class="text-danger">{{$errors->first('from_date')}}</div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">To Date<i class="text-danger" >*</i></label>
                    <div class="col-sm-8">
                        <input type="text" name="to_date" id="to_date" class="form-control" value="{{old('to_date')?old('to_date'):''}}" >
                        @if ($errors->has('to_date'))
                        <div class="text-danger">{{$errors->first('to_date')}}</div>
                        @endif
                    </div>
                </div>
                <div class="line"></div>
                <div class="form-group row">
                    <div class="col-sm-12 offset-sm-3">
                        <a href="{{url('/manage-users')}}" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-primary">Save changes</button> 
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("#from_date").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date('{{date("Y-m-d")}}'),
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#to_date").datepicker("option", "minDate", dt);
            }
        });
        $("#to_date").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date('{{date("Y-m-d")}}'),
            onSelect: function (selected) {
                var dt1 = new Date(selected);
                dt1.setDate(dt1.getDate() - 1);
                $("#from_date").datepicker("option", "maxDate", dt1);
            }
        });
    });

</script>
@endsection