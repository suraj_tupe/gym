@extends('layouts.admin')
@section('content')
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Edit Global Setting</h2>
    </div>
</header>
<!-- Dashboard Counts Section-->
<ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{url('/global-setting')}}">Global Setting</a></li>
              <li class="breadcrumb-item active">Edit Global Setting</li>
            </div>
          </ul>
<section class="dashboard-counts no-padding-bottom">
    <div class="container-fluid">
        <div class="row bg-white has-shadow">
            <div class="card-body">
                <form class="form-horizontal" action="{{url('/global-setting-post/'.base64_encode($setting_rec->id))}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Field</label>
                        <div class="col-sm-9">
                            <input type="text" name="field" id="field" class="form-control" readonly="" value="{{ucwords(implode(' ',explode('_',$setting_rec->field)))}}">
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Value</label>
                        <div class="col-sm-9">
                            @if($setting_rec->id==9)
                            <select name="value" id="value" class="form-control">
                                <option value="d/m/Y" @if($setting_rec->value=='d/m/Y') selected  @endif>DD/MM/YYYY</option>
                                <option value="d-m-Y" @if($setting_rec->value=='d-m-Y') selected  @endif>DD-MM-YYYY</option>
                                <option value="d/m/Y H:i:s" @if($setting_rec->value=='d/m/Y H:i:s') selected  @endif>DD/MM/YYYY HH:MM:SS</option>
                                <option value="d-m-Y H:i:s" @if($setting_rec->value=='d-m-Y H:i:s') selected  @endif>DD-MM-YYYY HH:MM:SS</option>
                                <option value="Y/d/m H:i:s" @if($setting_rec->value=='Y/d/m H:i:s') selected  @endif>YYYY/DD/MM HH:MM:SS</option>
                                <option value="Y-d-m H:i:s" @if($setting_rec->value=='Y-d-m H:i:s') selected  @endif>YYYY-DD-MM HH:MM:SS</option>
                            </select>
                            @else
                            <input type="text" name="value" id="value" class="form-control" value="{{$setting_rec->value}}">
                            @endif
                            @if ($errors->has('value'))
                            <div class="text-danger">{{$errors->first('value')}}</div>
                        @endif
                        </div>
                        
                        
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                        <div class="col-sm-12 offset-sm-3">
                            <a href="{{url('/global-setting')}}" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection