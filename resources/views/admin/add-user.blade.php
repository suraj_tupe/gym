@extends('layouts.admin')
@section('content')
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Edit Global Setting</h2>
    </div>
</header>
<!-- Dashboard Counts Section-->
<ul class="breadcrumb">
    <div class="container-fluid">
        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{url('/manage-users')}}">User List</a></li>
        <li class="breadcrumb-item active">Add User</li>
    </div>
</ul>
<section class="dashboard-counts no-padding-bottom">
    <div class="container-fluid">
        <div class="row bg-white has-shadow">
            <div class="card-body">
                <form class="form-horizontal" action="{{url('/add-user-post')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">First name<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="text" name="first_name" id="first_name" class="form-control" value="{{old('first_name')?old('first_name'):''}}">
                            @if ($errors->has('first_name'))
                            <div class="text-danger">{{$errors->first('first_name')}}</div>
                            @endif

                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Last name<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="text" name="last_name" id="last_name" class="form-control" value="{{old('last_name')?old('last_name'):''}}">
                            @if ($errors->has('last_name'))
                            <div class="text-danger">{{$errors->first('last_name')}}</div>
                            @endif

                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="text" name="user_email" id="user_email" class="form-control" value="{{old('user_email')?old('user_email'):''}}">
                            <small>(Optional)</small>
                            @if ($errors->has('user_email'))
                            <div class="text-danger">{{$errors->first('user_email')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Mobile Number<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="text" name="mobile_number" id="mobile_number" class="form-control" value="{{old('mobile_number')?old('mobile_number'):''}}">
                            @if ($errors->has('mobile_number'))
                            <div class="text-danger">{{$errors->first('mobile_number')}}</div>
                            @endif

                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Date of Birth<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="text" name="dob" id="dob" class="form-control" value="{{old('dob')?old('dob'):''}}" >
                            @if ($errors->has('dob'))
                            <div class="text-danger">{{$errors->first('dob')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Address<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <textarea name="address" id="address" class="form-control">{{old('address')?old('address'):''}}</textarea>
                            @if ($errors->has('address'))
                            <div class="text-danger">{{$errors->first('address')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Training Type<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="radio" class="training_type" name="training_type" id="training_type" value="0" @if(old('training_type')==0) checked="" @elseif(old('training_type')=='') checked="" @endif > Normal
                                   <input type="radio" class="training_type" name="training_type" id="training_type" value="1" @if(old('training_type')==1) checked="" @endif > Personal
                                   @if ($errors->has('training_type'))
                                   <div class="text-danger">{{$errors->first('training_type')}}</div>
                            @endif
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Payment Mode<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="radio" class="payment_mode" name="payment_mode" id="payment_mode" value="0" @if(old('payment_mode')==0) checked="" @elseif(old('payment_mode')=='')) checked="" @endif > Cash
                                   <input type="radio" class="payment_mode" name="payment_mode" id="payment_mode" value="1" @if(old('payment_mode')==1) checked="" @endif > Card
                                   <input type="radio" class="payment_mode" name="payment_mode" id="payment_mode" value="2"  @if(old('payment_mode')==2) checked="" @endif> Other
                                   <input type="text" class="form-control" name="other_type" id="other_type" style="display: none">
                            @if ($errors->has('payment_mode'))
                            <div class="text-danger">{{$errors->first('payment_mode')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Duration<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            @if(count($durations)>0)
                            @foreach($durations as $key=>$d)
                            <input type="radio" class="duration" name="duration" id="duration" value="{{$d->duration}}" @if($key==0) checked="" @endif> {{ ($d->duration==365)?'12':($d->duration)/30}} Month
                                   @endforeach
                                   @endif
                                   @if ($errors->has('payment_mode'))
                                   <div class="text-danger">{{$errors->first('payment_mode')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Offer Type<i class="text-danger" >*</i></label>
                        <div class="col-sm-8" >
                            <div id="offer_type_div" ></div>
                            @if ($errors->has('offer_type'))
                            <div class="text-danger">{{$errors->first('offer_type')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Total Amount<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="text" name="total_amount" id="total_amount" readonly="" class="form-control" >
                            @if ($errors->has('total_amount'))
                            <div class="text-danger">{{$errors->first('total_amount')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Discount Amount<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="text" name="discount_amount" id="discount_amount" class="form-control"  value="">
                            @if ($errors->has('discount_amount'))
                            <div class="text-danger">{{$errors->first('discount_amount')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Paid Amount<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="text" name="paid_amount" id="paid_amount" class="form-control">
                            @if ($errors->has('discount_amount'))
                            <div class="text-danger">{{$errors->first('discount_amount')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Balance Amount<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="text" name="balance_amount" id="balance_amount" class="form-control" readonly="">
                            @if ($errors->has('balance_amount'))
                            <div class="text-danger">{{$errors->first('balance_amount')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">From Date<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="text" name="from_date" id="from_date" class="form-control" >
                            @if ($errors->has('from_date'))
                            <div class="text-danger">{{$errors->first('from_date')}}</div>
                            @endif
                        </div>
                    </div>
                    <!--                    <div class="form-group row">
                                            <label class="col-sm-3 form-control-label">To Date<i class="text-danger" >*</i></label>
                                            <div class="col-sm-8">
                                                <input type="text" name="to_date" id="to_date" class="form-control" >
                                                @if ($errors->has('to_date'))
                                                <div class="text-danger">{{$errors->first('to_date')}}</div>
                                                @endif
                                            </div>
                                        </div>-->
                    <hr>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Offer Opted For<i class="text-danger" >*</i></label>
                        <div class="col-sm-8">
                            <input type="radio" name="offer_opted_for" id="offer_opted_for" value="0" checked=""> Single
                            <input type="radio" name="offer_opted_for" id="offer_opted_for" value="1"> Couple
                            <input type="radio" name="offer_opted_for" id="offer_opted_for" value="2"> Group
                            @if ($errors->has('offer_opted_for'))
                            <div class="text-danger">{{$errors->first('offer_opted_for')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                        <div class="col-sm-12 offset-sm-3">
                            <a href="{{url('/manage-users')}}" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-primary">Save changes</button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    $('.payment_mode').on('change', function () {
        var v = $(this).val();
        if (v == 2) {
            $('#other_type').show();
        } else {
            $('#other_type').val('');
            $('#other_type').hide();
        }
    });
    $(document).ready(function () {
        $("#from_date").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date('{{date("Y-m-d")}}'),
            onSelect: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#to_date").datepicker("option", "minDate", dt);
            }
        });
        $("#to_date").datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date('{{date("Y-m-d")}}'),
            onSelect: function (selected) {
                var dt1 = new Date(selected);
                dt1.setDate(dt1.getDate() - 1);
                $("#from_date").datepicker("option", "maxDate", dt1);
            }
        });
        $("#dob").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: "-70:+0",
        });
        $(document.body).on('change', '.offer-type-cls', function () {
            var offer_type = $('.offer-type-cls:checked').val();
            getOfferType(offer_type);
        });
        $(document.body).on('change', '#paid_amount', function () {
            var total_amount = $('#discount_amount').val();
            var paid_amount = $('#paid_amount').val();
            $('#balance_amount').val(total_amount - paid_amount);
        });
        $(document.body).on('change', '#discount_amount', function () {
            var total_amount = $('#discount_amount').val();
            var paid_amount = $('#paid_amount').val();
            if (paid_amount > 0) {
                $('#balance_amount').val(total_amount - paid_amount);
            }
        });
        var gym_type = $('.training_type:checked').val();
        var duration = $('.duration:checked').val();
        getGymType(gym_type, duration);
        $('.training_type').on('change', function () {
            var gym_type = $('.training_type:checked').val();
            var duration = $('.duration:checked').val();
            getGymType(gym_type, duration);
        });
        $('.duration').on('change', function () {
            var gym_type = $('.training_type:checked').val();
            var duration = $('.duration:checked').val();
            getGymType(gym_type, duration);
        });
    });
    function getOfferType(offer_type) {
        $('#total_amount').val('');
        $('#discount_amount').val('');
        $('#paid_amount').val('');
        $('#balance_amount').val('');
        $.ajax({
            url: '{{url("/get-offer-info")}}',
            type: 'post',
            dataType: 'json',
            data: {
                'offer_id': offer_type,
                '_token': '{{ csrf_token() }}',
            },
            success: function (msg) {
                $('#total_amount').val(msg.price);
                $('#discount_amount').val(msg.price);
            }
        })
    }
    function getGymType(gym_type, duration) {
        $('#total_amount').val('');
        $('#discount_amount').val('');
        $('#paid_amount').val('');
        $('#balance_amount').val('');
        $.ajax({
            url: '{{url("/get-gym-info")}}',
            type: 'post',
            dataType: 'json',
            data: {
                'gym_type': gym_type,
                duration: duration,
                '_token': '{{ csrf_token() }}',
            },
            success: function (msg) {
                $('#offer_type_div').html(msg.viewInfo);
                $('#total_amount').val(msg.price);
                $('#discount_amount').val(msg.price);
            }
        })
    }
</script>
@endsection