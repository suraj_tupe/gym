<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\UserInformation;
use App\TransInformations;
use App\Package;
use App\PackageFeature;


class ReportController extends Controller {

    public function getReport(){
        return view('admin.report-view');
    }
}
