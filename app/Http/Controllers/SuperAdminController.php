<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\UserInformation;
use App\TransInformations;
use App\Package;
use App\PackageFeature;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\GlobalSetting;

class SuperAdminController extends Controller {

    public function dashboard() {
        return view('admin.dashboard')->with('pageTitle', 'Dashbaord');
    }

    public function globalSetting() {

        return view('admin.global-setting')->with('pageTitle', 'Global Setting');
    }

    public function globalSettingData() {
        $all_rec = GlobalSetting::all();
        return \Yajra\DataTables\Facades\DataTables::of($all_rec)
                        ->editColumn('field', function($global) {
                            return ucwords(implode(' ', explode('_', $global->field)));
                        })
                        ->editColumn('action', function($global) {
                            return '<a href="' . url('/edit-global-setting/' . base64_encode($global->id)) . '"><i class="fa fa-pencil" ></i> Edit</a>';
                        })
                        ->make(true);
    }

    public function globalSettingEdit(Request $request) {
        $edit_id = $request->edit_id;
        $edit_id = base64_decode($edit_id);
        $setting_rec = GlobalSetting::find($edit_id);
        return view('admin.edit-global-setting', compact('setting_rec'))->with('pageTitle', 'Edit Global Setting');
    }

    public function globalSettingPost(Request $request) {
        $edit_id = $request->edit_id;
        $edit_id = base64_decode($edit_id);
        $custom_validation = '';
        if ($edit_id == '1') {
            // Site title validation
            $custom_validation = '|max:255';
        } else if ($edit_id == '2') {
            // Site email validation
            $custom_validation = '|email';
        } else if ($edit_id == '3') {
            // Site Contact email
            $custom_validation = '|email';
        } else if ($edit_id == '4') {
            //Meta title
            $custom_validation = '|max:255';
        } else if ($edit_id == '5') {
            //Meta Keyword
            $custom_validation = '|max:255';
        } else if ($edit_id == '6') {
            //Meta description
            $custom_validation = '|max:1000';
        } else if ($edit_id == '7') {
            //Logo Image
            $custom_validation = '|mimes:jpeg,jpg,png,gif';
        } else if ($edit_id == '8') {
            //Logo Title
            $custom_validation = '|max:255';
        } else if ($edit_id == '9') {
            //Date format
        }


        $validator = Validator::make($request->all(), [
                    'value' => 'required' . $custom_validation,
        ]);

        if ($validator->fails()) {
            return redirect('/edit-global-setting/' . base64_encode($edit_id))
                            ->withErrors($validator)
                            ->withInput();
        } else {
            $input = $request->all();
            $global_rec = GlobalSetting::find($edit_id);

            $global_rec->value = $input['value'];
            $global_rec->save();
            return redirect('/global-setting')->with('success', 'Global setting updated successfully.');
        }
    }

    public function getUsers() {
        return view('admin.user-list')->with('pageTitle', 'User List');
    }

    public function getUserList() {
        $all_rec = \App\User::where('type', '0')->get();
        return \Yajra\DataTables\Facades\DataTables::of($all_rec)
                        ->editColumn('name', function($user) {
                            return ucwords($user->userInfo->first_name . ' ' . $user->userInfo->last_name);
                        })
                        ->editColumn('mobile', function($user) {
                            return $user->userInfo->mobile;
                        })
//                        ->editColumn('offer_type', function($user) {
//                            return ($user->userInfo->transInfo->offer_type == 0) ? 'Gym' : (($user->userInfo->transInfo->offer_type == 1) ? 'Gym + Cardio' : 'Gym + Cardio + Crossfit');
//                        })
//                        ->editColumn('paid_amount', function($user) {
//                            return $user->userInfo->transInfo->paid_amount;
//                        })
//                        ->editColumn('balance_amount', function($user) {
//                            return $user->userInfo->transInfo->balance_amount;
//                        })
                        ->editColumn('to_date', function($user) {
                            return date('d-m-Y', strtotime($user->userInfo->transInfo->last()->to_date));
                        })
                        ->editColumn('action', function($user) {
                            return '<a href="' . url('/edit-user/' . base64_encode($user->id)) . '"><i class="fa fa-pencil" ></i> Edit</a>';
                        })
                        ->make(true);
    }

    public function editUser(Request $request) {
        $edit_id = base64_decode($request->edit_id);
        $user_details = \App\User::where('type', '0')->where('id', $edit_id)->first();
        if (count($user_details) <= 0) {
            return redirect()->with('error', 'Sorry! no record found');
        }
        $gym_type = 0;
        $durations = Package::groupBy('duration')->get();
//        $arr_gym_type=$user_details->userInfo->transInfo;
//        if(count($arr_gym_type)>0) {
//            foreach ($arr_gym_type as $get_gym) {
//                $gym_type=$get_gym->gymInfo->gym_type;
//                $gym_duration=$get_gym->gymInfo->gym_type;
//            }
//        }
//        $gym_records = Package::where('gym_type', $gym_type)->where('duration',$user_details->userInfo->transInfo->gymInfo->duration)->groupBy('name')->get();
        return view('admin.edit-user', compact('user_details', 'edit_id', 'durations'))->with('pageTitle', 'Edit User');
    }

    public function getRenew() {
        $durations = Package::groupBy('duration')->get();
        $view = \Illuminate\Support\Facades\View::make('admin.get-renew', compact('durations'));
        $contents = $view->render();
        print_r(json_encode(array('viewInfo' => $contents)));
    }

    public function editUserPost(Request $request) {
        $edit_id = base64_decode($request->edit_id);
        $user_details = User::where('type', '0')->where('id', $edit_id)->first();
        if (count($user_details) <= 0) {
            return redirect()->with('error', 'Sorry! no record found');
        }

        $validator = Validator::make($request->all(), [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'user_email' => 'email|max:255',
                    'mobile_number' => 'required|min:10|max:11',
                    'address' => 'required',
                    'dob' => 'required',
                    'balance_amount' => 'required',
                    'from_date' => 'required',
//                    'to_date' => 'required',
                    'training_type' => 'required',
                    'payment_mode' => 'required',
                    'offer_type' => 'required',
                    'paid_amount' => 'required',
                    'discount_amount' => 'required',
                    'offer_opted_for' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('/edit-user/' . base64_encode($edit_id))
                            ->withErrors($validator)
                            ->withInput();
        } else {
            $input = $request->all();
//            dump($input);
            $userInformation = UserInformation::where('user_id', $user_details->id)->first();
            if (count($userInformation) <= 0) {
                return redirect()->with('error', 'Sorry! no record found');
            }
            $userInformation->first_name = $input['first_name'];
            $userInformation->last_name = $input['last_name'];
            $userInformation->mobile = $input['mobile_number'];
            $userInformation->dob = $input['dob'];
            $userInformation->address = $input['address'];
            $userInformation->save();
            $trans_rec = TransInformations::where('information_id', $userInformation->id)->first();
//            dd($trans_rec);
            if (isset($input['payment_mode']) && isset($input['total_amount']) && count($trans_rec)) {
                if (strtotime($trans_rec->to_date) <= strtotime(date('Y-m-d'))) {
                    $obj2 = new TransInformations();
                    $obj2->insert([
                        'information_id' => $userInformation->id,
                        'offer_type' => $input['offer_type'],
                        'payment_mode' => $input['payment_mode'],
                        'other_payment_mode' => $input['other_type'],
                        'total_fees' => $input['total_amount'],
                        'paid_amount' => $input['paid_amount'],
                        'balance_amount' => ($input['discount_amount'] - $input['paid_amount']),
                        'discount_amount' => $input['discount_amount'],
                        'from_date' => date('Y-m-d', strtotime($input['from_date'])),
                        'to_date' => date('Y-m-d', strtotime('+' . (($input['duration'] == 365) ? "12" : ($input['duration'] / 30)) . ' month ', strtotime($input['from_date']))),
                        'duration' => $input['duration'],
                    ]);
                }
                return redirect('/manage-users')->with('success', 'User updated successfully.');
            }
            return redirect('/manage-users')->with('success', 'User Information updated successfully');
        }
    }

    public function addUser(Request $request) {
        $durations = Package::groupBy('duration')->get();
        return view('admin.add-user', compact('durations'))->with('pageTitle', 'Add User');
    }

    public function addUserPost(Request $request) {
        $validator = Validator::make($request->all(), [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'user_email' => 'email|max:255',
                    'mobile_number' => 'required|min:10|max:11',
                    'address' => 'required',
                    'dob' => 'required',
                    'balance_amount' => 'required',
                    'from_date' => 'required',
//                    'to_date' => 'required',
                    'training_type' => 'required',
                    'payment_mode' => 'required',
                    'offer_type' => 'required',
                    'paid_amount' => 'required',
                    'discount_amount' => 'required',
                    'offer_opted_for' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('/add-user')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            $input = $request->all();
//            dd(date('Y-m-d',  strtotime('+'.(($input['duration']==365)?"12":($input['duration']/30)).' month ',  strtotime($input['from_date']))));
//            dd($input);
            $user_obj = new User();
            $user_id = $user_obj->insertGetId([
                'email' => $input['user_email'],
                'name' => $input['first_name'] . ' ' . $input['last_name'],
            ]);
            if ($user_id > 0) {
                $obj = new UserInformation();
                $id = $obj->insertGetId([
                    'user_id' => $user_id,
                    'first_name' => $input['first_name'],
                    'last_name' => $input['last_name'],
                    'mobile' => $input['mobile_number'],
                    'dob' => date('Y-m-d', strtotime($input['dob'])),
                    'address' => $input['address'],
                    'offer_opted_for' => $input['offer_opted_for'],
                ]);

                if ($id > 0) {
                    $obj2 = new TransInformations();
                    $obj2->insert([
                        'information_id' => $id,
                        'offer_type' => $input['offer_type'],
                        'payment_mode' => $input['payment_mode'],
                        'other_payment_mode' => $input['other_type'],
                        'total_fees' => $input['total_amount'],
                        'paid_amount' => $input['paid_amount'],
                        'balance_amount' => ($input['discount_amount'] - $input['paid_amount']),
                        'discount_amount' => $input['discount_amount'],
                        'from_date' => date('Y-m-d', strtotime($input['from_date'])),
                        'to_date' => date('Y-m-d', strtotime('+' . (($input['duration'] == 365) ? "12" : ($input['duration'] / 30)) . ' month ', strtotime($input['from_date']))),
                        'duration' => $input['duration'],
                    ]);
                    return redirect('/manage-users')->with('success', 'User added successfully.');
                } else {
                    return redirect('/add-user')->with('error', 'Sorry! Something went wrong, Please try again.');
                }
            } else {
                return redirect('/add-user')->with('error', 'Sorry! Something went wrong, Please try again.');
            }
            return redirect('/manage-users')->with('success', 'User Information added successfully');
        }
    }

    public function offerInformation(Request $request) {
        $offer_id = $request->input('offer_id');
        $gym_records = Package::where('id', $offer_id)->first();
        print_r(json_encode(array('price' => (count($gym_records) > 0 ? $gym_records->price : '0'))));
    }

    public function gymInformation(Request $request) {
        $gym_type = $request->input('gym_type');
        $selected_id = $request->input('selected_id');
        $duration = $request->input('duration');
        $gym_records = Package::where('gym_type', $gym_type)->where('duration', $duration)->groupBy('name')->get();
        $first_records = Package::where('gym_type', $gym_type)->where('duration', $duration)->groupBy('name')->first();
        $view = \Illuminate\Support\Facades\View::make('admin.gym_types', compact('gym_records', 'selected_id'));
        $contents = $view->render();
        print_r(json_encode(array('viewInfo' => $contents, 'price' => isset($first_records->price) ? $first_records->price : '')));
    }

}
