<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller {

    public function index() {
        return view('admin_login');
    }

    public function adminLogin(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            // Valid user
            $input = $request->all();
            $admin_info = User::where('email', $input['email'])->first();
            if (count($admin_info) > 0) {
                if (Hash::check($input['password'], $admin_info->password)) {
                    // The passwords match...
                    Session::put('user_id',$admin_info->id);
                    Session::put('email',$admin_info->email);
                    return redirect('/dashboard')->with('success', 'Welcome Admin');
                } else {
                    return redirect('/admin')->with('error', 'Sorry! no credential found');
                }
            } else {
                return redirect('/admin')->with('error', 'Sorry! no credential found');
            }
        }

    }
    
    
}
