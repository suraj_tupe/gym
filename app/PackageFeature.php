<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageFeature extends Model
{
    protected $fillable = [
      'package_id','feature_name',
    ];
    
    public function features() {
        return $this->belongsTo('App\Package','package_id','id');
    }
}
