<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    protected $fillable = [
      'user_id','first_name','last_name',  'mobile', 'address','dob','offer_opted_for',
    ];
    
    public function userInfo() {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function transInfo() {
        return $this->hasMany('App\TransInformations','information_id','id');
    }

}
