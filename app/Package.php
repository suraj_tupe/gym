<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
      'name','price','gym_type','duration',
    ];
    
    public function features() {
        return $this->hasMany('App\PackageFeature','package_id','id');
    }
    public function gymInfo() {
        return $this->hasOne('App\Package','offer_type','id');
    }
}
