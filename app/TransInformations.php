<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransInformations extends Model
{
    protected $fillable = [
      'offer_type','information_id','payment_mode','other_payment_mode','total_fees','paid_amount','balance_amount','discount_amount','from_date','to_date','duration',
    ];
    
    public function infoUser() {
        return $this->belongsTo('App\UserInformation','information_id','id');
    }
    public function gymInfo() {
        return $this->belongsTo('App\Package','offer_type','id');
    }
    
}
