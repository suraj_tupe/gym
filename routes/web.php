<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin','AdminController@index' );
Route::post('/admin-login','AdminController@adminLogin' );

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/dashboard', 'SuperAdminController@dashboard');
Route::get('/global-setting', 'SuperAdminController@globalSetting');
Route::get('/get-global-setting', 'SuperAdminController@globalSettingData');
Route::get('/edit-global-setting/{edit_id}', 'SuperAdminController@globalSettingEdit');
Route::post('/global-setting-post/{edit_id}', 'SuperAdminController@globalSettingPost');
Route::get('/manage-users', 'SuperAdminController@getUsers');
Route::get('/get-user-list', 'SuperAdminController@getUserList');
Route::get('/edit-user/{edit_id}', 'SuperAdminController@editUser');
Route::post('/get-renew', 'SuperAdminController@getRenew');
Route::post('/edit-user-post/{edit_id}', 'SuperAdminController@editUserPost');
Route::get('/add-user', 'SuperAdminController@addUser');
Route::post('/add-user-post', 'SuperAdminController@addUserPost');
Route::post('/get-offer-info', 'SuperAdminController@offerInformation');
Route::post('/get-gym-info', 'SuperAdminController@gymInformation');
Route::get('/manage-report', 'ReportController@getReport');
